package com.moumene.customerservice.services;

import com.moumene.customerservice.dtos.CustomerRequestDTO;
import com.moumene.customerservice.dtos.CustomerResponseDTO;

import java.util.List;

public interface CustomerService {


    List<CustomerResponseDTO> getAllCustomers();

    CustomerResponseDTO getCustomerById(String id);

    CustomerResponseDTO addCustomer(CustomerRequestDTO customerRequestDTO);

    CustomerResponseDTO updateCustomer(String id, CustomerRequestDTO customerRequestDTO);

    void deleteCustomer(String id);
}
