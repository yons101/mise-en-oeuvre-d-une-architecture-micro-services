package com.moumene.customerservice;

import com.moumene.customerservice.dtos.CustomerRequestDTO;
import com.moumene.customerservice.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CustomerServiceApplication {

    @Autowired
    CustomerService customerService;

    public static void main(String[] args) {
        SpringApplication.run(CustomerServiceApplication.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner() {
        return args -> {
            customerService.addCustomer(new CustomerRequestDTO("Younes", "younes@gmail.com"));
            customerService.addCustomer(new CustomerRequestDTO("Yassine", "yassine@gmail.com"));
            customerService.addCustomer(new CustomerRequestDTO("Ahmed", "ahmed@gmail.com"));
        };
    }

}
