package com.moumene.customerservice.mappers;


import com.moumene.customerservice.dtos.CustomerRequestDTO;
import com.moumene.customerservice.dtos.CustomerResponseDTO;
import com.moumene.customerservice.entities.Customer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

    CustomerResponseDTO customerToCustomerResponseDTO(Customer customer);

    Customer customerRequestDTOToCustomer(CustomerRequestDTO customerRequestDTO);


}
