package com.moumene.billingservice.entities;

import lombok.Data;

@Data
public class Customer {
    String id;
    String name;
    String email;
}
