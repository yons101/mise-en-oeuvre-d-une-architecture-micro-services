package com.moumene.billingservice.services;

import com.moumene.billingservice.dtos.InvoiceRequestDTO;
import com.moumene.billingservice.dtos.InvoiceResponseDTO;
import com.moumene.billingservice.entities.Invoice;
import com.moumene.billingservice.exceptions.InvoiceNotFoundException;
import com.moumene.billingservice.mappers.InvoiceMapper;
import com.moumene.billingservice.openfeign.CustomerRestClient;
import com.moumene.billingservice.repositories.InvoiceRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional
public class InvoiceServiceImpl implements InvoiceService {

    private InvoiceRepository invoiceRepository;
    private InvoiceMapper invoiceMapper;
    private CustomerRestClient customerRestClient;

    @Override
    public List<InvoiceResponseDTO> getAllInvoices() {
        return invoiceRepository.findAll()
                .stream()
                .map(invoice -> {
                    InvoiceResponseDTO invoiceResponseDTO = invoiceMapper.InvoiceToInvoiceResponseDTO(invoice);
                    invoiceResponseDTO.setCustomer(customerRestClient.getCustomerById(invoice.getCustomerId()));
                    return invoiceResponseDTO;
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<InvoiceResponseDTO> getAllInvoicesByCustomerId(String customerId) {
        return invoiceRepository.findByCustomerId(customerId)
                .stream()
                .map(invoice -> {
                    InvoiceResponseDTO invoiceResponseDTO = invoiceMapper.InvoiceToInvoiceResponseDTO(invoice);
                    invoiceResponseDTO.setCustomer(customerRestClient.getCustomerById(invoice.getCustomerId()));
                    return invoiceResponseDTO;
                })
                .collect(Collectors.toList());
    }

    @Override
    public InvoiceResponseDTO getInvoiceById(String id) {
        Invoice invoice = invoiceRepository.findById(id).orElseThrow(() -> new InvoiceNotFoundException(id));
        InvoiceResponseDTO invoiceResponseDTO = invoiceMapper.InvoiceToInvoiceResponseDTO(invoice);
        invoiceResponseDTO.setCustomer(customerRestClient.getCustomerById(invoice.getCustomerId()));
        return invoiceMapper.InvoiceToInvoiceResponseDTO(invoice);
    }

    @Override
    public InvoiceResponseDTO addInvoice(InvoiceRequestDTO InvoiceRequestDTO) {
        Invoice invoice = invoiceMapper.InvoiceReuestDTOToInvoice(InvoiceRequestDTO);
        invoice.setId(UUID.randomUUID().toString());
        invoice.setDate(new Date());
        invoiceRepository.save(invoice);
        InvoiceResponseDTO invoiceResponseDTO = invoiceMapper.InvoiceToInvoiceResponseDTO(invoice);
        invoiceResponseDTO.setCustomer(customerRestClient.getCustomerById(InvoiceRequestDTO.getCustomerId()));
        return invoiceResponseDTO;
    }

    @Override
    public InvoiceResponseDTO updateInvoice(String id, InvoiceRequestDTO InvoiceRequestDTO) {
        Invoice invoice = invoiceRepository.findById(id).orElseThrow(() -> new InvoiceNotFoundException(id));
        invoice.setAmount(InvoiceRequestDTO.getAmount());
        invoice.setCustomerId(InvoiceRequestDTO.getCustomerId());
        InvoiceResponseDTO invoiceResponseDTO = invoiceMapper.InvoiceToInvoiceResponseDTO(invoice);
        invoiceResponseDTO.setCustomer(customerRestClient.getCustomerById(InvoiceRequestDTO.getCustomerId()));
        return invoiceResponseDTO;
    }

    @Override
    public void deleteInvoice(String id) {
        Invoice invoice = invoiceRepository.findById(id).orElseThrow(() -> new InvoiceNotFoundException(id));
        invoiceRepository.delete(invoice);
    }


}
