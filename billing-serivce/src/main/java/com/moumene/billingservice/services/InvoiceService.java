package com.moumene.billingservice.services;

import com.moumene.billingservice.dtos.InvoiceRequestDTO;
import com.moumene.billingservice.dtos.InvoiceResponseDTO;

import java.util.List;

public interface InvoiceService {


    List<InvoiceResponseDTO> getAllInvoices();

    List<InvoiceResponseDTO> getAllInvoicesByCustomerId(String customerId);

    InvoiceResponseDTO getInvoiceById(String id);

    InvoiceResponseDTO addInvoice(InvoiceRequestDTO invoiceRequestDTO);

    InvoiceResponseDTO updateInvoice(String id, InvoiceRequestDTO invoiceRequestDTO);

    void deleteInvoice(String id);
}
