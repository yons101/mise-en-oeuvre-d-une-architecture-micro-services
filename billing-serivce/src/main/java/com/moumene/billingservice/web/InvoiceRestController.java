package com.moumene.billingservice.web;

import com.moumene.billingservice.dtos.InvoiceRequestDTO;
import com.moumene.billingservice.dtos.InvoiceResponseDTO;
import com.moumene.billingservice.services.InvoiceService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class InvoiceRestController {

    private InvoiceService InvoiceService;

    @GetMapping("/invoices")
    public List<InvoiceResponseDTO> Invoices() {
        return InvoiceService.getAllInvoices();
    }

    @GetMapping("/invoices/{id}")
    public InvoiceResponseDTO getInvoice(@PathVariable String id) {
        return InvoiceService.getInvoiceById(id);
    }

    @PostMapping("/invoices")
    public InvoiceResponseDTO saveInvoice(InvoiceRequestDTO InvoiceRequestDTO) {
        return this.InvoiceService.addInvoice(InvoiceRequestDTO);
    }

    @PutMapping("/invoices/{id}")
    public InvoiceResponseDTO updateInvoice(@PathVariable String id, @RequestBody InvoiceRequestDTO InvoiceRequestDTO) {
        return InvoiceService.updateInvoice(id, InvoiceRequestDTO);
    }

    @DeleteMapping("/invoices/{id}")
    public void deleteInvoice(@PathVariable String id) {
        this.InvoiceService.deleteInvoice(id);
    }

    @GetMapping("/invoicesbycustomer/{id}")
    public List<InvoiceResponseDTO> invoicesByCustomerId(@PathVariable String id) {
        return InvoiceService.getAllInvoicesByCustomerId(id);
    }


}
