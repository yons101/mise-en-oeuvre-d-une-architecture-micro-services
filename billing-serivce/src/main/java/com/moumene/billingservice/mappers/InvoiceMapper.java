package com.moumene.billingservice.mappers;


import com.moumene.billingservice.dtos.InvoiceRequestDTO;
import com.moumene.billingservice.dtos.InvoiceResponseDTO;
import com.moumene.billingservice.entities.Invoice;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface InvoiceMapper {

    InvoiceResponseDTO InvoiceToInvoiceResponseDTO(Invoice Invoice);

    Invoice InvoiceReuestDTOToInvoice(InvoiceRequestDTO InvoiceRequestDTO);


}
