
## Architecture


![img](https://i.imgur.com/W3FOqun.png)

 ## Customers microservice

![img](https://i.imgur.com/HEP32zj.png)

  

![img](https://i.imgur.com/OS6O3Ja.png)

   ## Billing microservice

![img](https://i.imgur.com/P3o4Hyw.png)

  
   ## Postman

![img](https://i.imgur.com/5XFkFPm.png)

  
![img](https://i.imgur.com/Fumbwtx.png)

   ## Eureka discovery service

![img](https://i.imgur.com/iZQCMAO.png)

  ## Docker images

![img](https://i.imgur.com/dKsNceS.png)